/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ทักช์ติโชค
 */
public class UserServics {

    private static ArrayList<User> userList = new ArrayList<>();

    // Popup
    static {
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
    }

    //Create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    //Updata
    public static boolean updataUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    //Read 1 user
    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);
    }

    //Read all 1 users
    public static ArrayList<User> getUsers() {
        return userList;
    }

    //Search userName
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return userList;
    }

    //Delece user
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    //Delece user
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    //Login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save() {
        FileOutputStream fos = null;
        try {
            File file = new File("users.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                fos.close();
            } catch (IOException ex) {

            }
        }
    }

    public static void load() {
        FileInputStream fis = null;
        try {

            File file = new File("users.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }

            } catch (IOException ex) {

            }
        }
    }
}
